﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerGrab : MonoBehaviour {
    public Controller controller;
    public GameObject[] snowBalls;
    public List<Collider> colliderList = new List<Collider>();
    public bool onSnowground = false;

	// Use this for initialization
	void Start () {
        controller = gameObject.transform.parent.GetComponent<Controller>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.S))
        {
            CreateSnowball(new Vector3(0, 2));
        }
	}

    public void GrabItems(GameObject contr)
    {
        Spawner spwn = null;
        foreach(Collider coll in colliderList)
        {
            if(coll.gameObject.tag == "Item" || coll.gameObject.tag == "SnowItems" || coll.gameObject.tag == "SmallItems")
            {
                foreach(Transform child in coll.gameObject.transform)
                {
                    if(child.gameObject.tag == "SnowItems")
                    {
                        child.parent = null;
                    }
                }
                Transform parent = coll.transform.parent;
                if(parent != null)
                {
                    Collider[] parentColl = parent.gameObject.GetComponents<Collider>();
                    foreach(Collider parColl in parentColl)
                    {
                        Physics.IgnoreCollision(coll, parColl, false);
                    }
                }
                coll.gameObject.transform.SetParent(contr.transform);
                Snowball snowball = coll.gameObject.GetComponent<Snowball>();
                
                Rigidbody tmpRigidBody = coll.gameObject.GetComponent<Rigidbody>();
                if (tmpRigidBody != null)
                {
                    tmpRigidBody.constraints = RigidbodyConstraints.FreezeRotation;
                    tmpRigidBody.useGravity = false;
                    
                }
            }
            if(coll.gameObject.tag == "Spawner" && controller.menu.activeSelf && controller.menu.transform.parent != controller.gameObject.transform)
            {
                spwn = coll.gameObject.GetComponent<Spawner>();
            }
        }
        if(spwn != null)
        {
            spawnItem(spwn.template);
        }
    }

    void spawnItem(GameObject obj)
    {
        GameObject copy = Instantiate(obj);
        copy.SetActive(true);
        copy.transform.parent = controller.gameObject.transform;
        SmallItem item = copy.GetComponent<SmallItem>();
        Vector3 pos = Vector3.zero;
        if(item != null)
            pos = item.spawnPos;
        copy.transform.localPosition = pos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!colliderList.Contains(other))
        {
            if (other.gameObject.tag == "GameController") 
            {
                Controller otherCont = other.gameObject.GetComponent<ControllerGrab>().controller;
                if(onSnowground == true && controller.triggerPressed && otherCont != null && otherCont.triggerPressed)
                    CreateSnowball(other.transform.position);
            }
            Item item = other.gameObject.GetComponent<Item>();
            if (item != null)
            {
                if (controller.gameObject.name.Contains("right"))
                    item.touchRight = true;
                if (controller.gameObject.name.Contains("left"))
                    item.touchLeft = true;
            }
            colliderList.Add(other);
        }
        if (!onSnowground && other.tag == "Snowground")
        {
            onSnowground = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (colliderList.Contains(other))
        {
            colliderList.Remove(other);
        }
        if (other.gameObject.tag == "SnowItems")
        {
            Item item = other.GetComponent<Item>();
            if (controller.gameObject.name.Contains("right"))
                item.touchRight = true;
            if (controller.gameObject.name.Contains("left"))
                item.touchLeft = true;
        }
        if (onSnowground && other.tag =="Snowground")
        {
            onSnowground = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collided with: " + collision.transform.name);
        
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Snowground")
            onSnowground = false;
    }

    private void CreateSnowball(Vector3 pos)
    {
        Debug.Log("Create new SnowBall");
        if (gameObject.transform.parent.name == "Controller (right)")
        {
            if(snowBalls.Length > 0)
            {
                int ballNo = Random.Range(0, snowBalls.Length);
                if (snowBalls[ballNo] != null)
                {
                    GameObject newBall = Instantiate(snowBalls[ballNo]);
                    newBall.transform.position = pos;
                    newBall.transform.localScale = snowBalls[ballNo].transform.lossyScale;
                    //newBall.transform.parent = gameObject.transform.parent;
                    newBall.SetActive(true);
                }
            }
        }
    }
}
