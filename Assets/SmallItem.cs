﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallItem : MonoBehaviour {
    Rigidbody rBody;
    Collider coll;
    public Vector3 spawnPos;
	// Use this for initialization
	void Start () {
        rBody = gameObject.GetComponent<Rigidbody>();
        coll = gameObject.GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionStay(Collision collision)
    {
        if(collision.gameObject.tag == "SnowItems" && transform.parent == null)
        {
            Collider[] colls = collision.gameObject.GetComponents<Collider>();
            foreach(Collider co in colls)
            {
                Physics.IgnoreCollision(coll, co);
            }
            gameObject.transform.parent = collision.gameObject.transform;
            rBody.isKinematic = true;
            rBody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
        }   
    }
}
