﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Controller : MonoBehaviour {
    SteamVR_TrackedObject trackedObject;
    public bool triggerPressed;
    public bool menuPressed;
    public ControllerGrab grabScript;
    public GameObject model;
    public GameObject menu;
    public Vector3 teleportPos;
    public GameObject pointer;
    public GameObject trackingSpace;
    // Use this for initialization
    Vector3 lastPos;
	void Start () {
        trackedObject = gameObject.GetComponent<SteamVR_TrackedObject>();
        grabScript = gameObject.GetComponentInChildren<ControllerGrab>();
        model = gameObject.transform.GetChild(0).gameObject;
        trackingSpace = gameObject.transform.parent.gameObject;
    }

    private void FixedUpdate()
    {
        
    }

    // Update is called once per frame
    void Update () {
        SteamVR_Controller.Device controller = SteamVR_Controller.Input((int)trackedObject.index);
        if (controller.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            if (!triggerPressed)
            {
                grabScript.GrabItems(gameObject);
                triggerPressed = true;
            }
        }
        else
        {
            if (triggerPressed)
            {
                ReleaseGrab();
                triggerPressed = false;
            }
        }
        if (controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))
        {
            /*
            Vector3 telPos = teleportPos;
            telPos.y = trackingSpace.transform.position.y;
            trackingSpace.transform.position = telPos;
            pointer.SetActive(false);*/
        }
        if (controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))
        {
            //point();
        }

        if (controller.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            switchMenu();
        }
        if (controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            menuPressed = false;
        }
        lastPos = gameObject.transform.position;
    }

    void point()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.forward, 100);
        teleportPos = trackingSpace.transform.position;
        foreach (RaycastHit hit in hits)
        {
            GameObject hitObj = hit.transform.gameObject;
            if(hitObj.name == "Snowground" && pointer != null)
            {
                pointer.SetActive(true);
                Vector3 hitPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                pointer.transform.position = hitPos;
                teleportPos = hitPos;
            }
        }
    }

    void switchMenu()
    {
        if(menu.activeSelf)
        {
            hideMenu();
        }
        else
        {
            showMenu();
        }
    }

    void showMenu()
    {
        if(menu != null)
        {
            menu.transform.parent = gameObject.transform;
            Vector3 menPos = Vector3.zero;
            menPos.y = -0.25f;
            menPos.z = 0.08f;
            menu.transform.localRotation = Quaternion.Euler(0, 0, 0);
            menu.transform.localPosition = menPos;
            menu.SetActive(true);
        }
    }

    void hideMenu()
    {
        if(menu != null)
        {
            menu.transform.parent = null;
            menu.SetActive(false);
        }
    }

    void ReleaseGrab()
    {
        Vector3 vel = transform.position - lastPos;
        vel = vel / Time.deltaTime;
        //Vector3 velocity = currPos * Time.deltaTime;
        Debug.Log("Child count: " + transform.childCount);
        foreach(Transform child in gameObject.transform)
        {
            if(child.gameObject.name != "Model" && child.tag != "Spawner")
            {
                child.transform.SetParent(null);
                Rigidbody childBody = child.gameObject.GetComponent<Rigidbody>();
                if (childBody != null)
                {
                    childBody.useGravity = true;
                    childBody.constraints = RigidbodyConstraints.None;
                    if(child.gameObject.tag == "SmallItems")
                        childBody.isKinematic = false;
                    childBody.velocity = vel;
                }
            }
        }
    }
}
