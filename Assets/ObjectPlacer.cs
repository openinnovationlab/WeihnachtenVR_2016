﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacer : MonoBehaviour {
    public GameObject template;
    public static bool drawGizmo = true;
	// Use this for initialization
	void Start () {
		if(template != null)
        {
            GameObject dup = Instantiate(template);
            dup.transform.position = gameObject.transform.position;
            dup.SetActive(true);
            dup.transform.SetParent(gameObject.transform);
        }
	}

    private void Update()
    {

    }

    private void OnDrawGizmos()
    {
        if (drawGizmo && template != null)
        {
            MeshFilter[] filters = template.GetComponentsInChildren<MeshFilter>();
            foreach (MeshFilter filt in filters)
            {
                if (filt.sharedMesh != null)
                {
                    Vector3 pos = gameObject.transform.position;
                    pos.y += filt.sharedMesh.bounds.size.y / 2;
                    Gizmos.DrawMesh(filt.sharedMesh, pos);
                }
            }
        }
    }
}
