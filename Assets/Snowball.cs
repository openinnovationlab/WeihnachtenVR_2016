﻿using UnityEngine;
using System.Collections;

public class Snowball : MonoBehaviour {
    public float scale = 0.2f;
    Vector3 lastPos;
    Vector3 velocity;
    public Vector3 collPos;
    Rigidbody rBody;
    public bool touchControllerLeft = false;
    public bool touchControllerRight = false;
    public float stddrag;
    public float stdangdrag;
    public bool coll = false;
    // Use this for initialization
    void Start () {
        lastPos = transform.position;
        rBody = gameObject.GetComponent<Rigidbody>();
        mass = rBody.mass;
        losScale = gameObject.transform.localScale;
        stddrag = rBody.drag;
        stdangdrag = rBody.angularDrag;
	}
    float time = 0;
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        velocity = transform.position - lastPos;
        velocity.x = Mathf.Abs(velocity.x);
        velocity.y = Mathf.Abs(velocity.y);
        velocity.z = Mathf.Abs(velocity.z);
        
        lastPos = transform.position;
	}

    float mass;
    Vector3 losScale;
    private void OnTriggerStay(Collider other)
    {
       if (other.gameObject.tag == "Snowground" && (velocity.x > 0.01 || velocity.z > 0.01))
       {
            float rescale = scale * Time.deltaTime;
            losScale.z += rescale;
            losScale.x += rescale;
            losScale.y += rescale;
            gameObject.transform.Rotate(-velocity * 100);
            mass = mass * scale * Time.deltaTime;
            Vector3 lossyScale = gameObject.transform.lossyScale;
            if (time > 0.5 && lossyScale.x < losScale.x && lossyScale.y < losScale.y)
            {
                gameObject.transform.localScale = losScale;
                rBody.mass = mass;
                time = 0;
            }
       }
    }

    Vector3 getDirRot(Vector3 rot)
    {
        float x = rot.x;
        float y = rot.y;
        float z = rot.z;
        Vector3 ret = new Vector3(z,y,x);
        return ret;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            //rBody.constraints = RigidbodyConstraints.FreezePositionY;
            rBody.angularDrag = 200;
            coll = true;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        
        if (collision.gameObject.tag == "Ground" && rBody.drag <= 100)
            rBody.drag += 20 * Time.deltaTime;
           
        Snowball sn = collision.gameObject.GetComponent<Snowball>();
        if (collision.gameObject.tag == "SnowItems" && sn != null)
        {
            Vector3 thisPos = gameObject.transform.position;
            Vector3 otPos = collision.transform.position;
            Vector3 collSize = collision.collider.bounds.size;
            float rangeX = collSize.x * 0.3f;
            float rangeY = collSize.z * 0.3f;
            if (thisPos.y > otPos.y)
                if (inRange(thisPos.x, otPos.x, rangeX) && inRange(thisPos.z, otPos.z, rangeY) && transform.parent == null)
                {
                    Debug.Log("Stack");
                    rBody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                    gameObject.transform.parent = collision.gameObject.transform;
                }
        }
    }

    private bool inRange(float thisVal, float otherVal, float range)
    {
        if (otherVal - range < thisVal && thisVal < otherVal + range)
            return true;
        else
            return false;
    }

    private void resetPhysics()
    {
        rBody.constraints = RigidbodyConstraints.None;
        rBody.drag = stddrag;
        rBody.angularDrag = stdangdrag;
        coll = false;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            resetPhysics();
        }
    }
}
